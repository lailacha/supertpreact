import logo from './logo.svg';
import MyComponent from './components/MyComponent';
import './App.css';

function App() {
  return (
    <div className="App">
     <MyComponent></MyComponent>
    </div>
  );
}

export default App;
