import React from 'react'

export default function MyComponent() {
    const prenom = "Edith"; // Ajoute ton prenom
    const nom = "Francois" //Ajoute ton nom
    const age = ""; //Ajoute ton age
    const option = "SLAM"; //Ajoute ta spé
    const optionPhrase = (option === "SLAM") ? "Tu es dans la meilleure spécialité" : "Berk";

    return (
        <div className="container rounded-pill bg-dark text-white w-50 p-5 mt-5 ">
          <h2 className='mt-3'>Bonjour {prenom} {nom} 😀 </h2>
         {(age) ? <h3>Tu as {age} ans</h3> : ""} 
          <p>{optionPhrase}</p>
        </div>
    )
}
